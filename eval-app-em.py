#!python3
# -*- coding: utf-8 -*-
# pylint: disable=W0312, line-too-long, C0103
"""
Formalizing Differential Privacy as Bayesian Networks
Alexander Motzek, Data Insight Lab, Lufthansa Industry Solutions
alexander.motzek@lhind.dlh.de

Evaluation application for executing all experiments mentioned in the paper submitted for review to KDD 2018.
"""
import bayesnet
import numpy as np

import json
import pandas as pd

import itertools as it

import tqdm
import argparse

def calculate_alpha(n,epsilon):
    return n / ( np.exp(epsilon) - 1 + n )


def runExperiment(samplerange, emiterations, epsilons, nnodes, cardinality):
    experimentResults = []
    resultLine = {}

    resultLine["emIterations"] = emiterations
    resultLine["nodes"] = nnodes
    resultLine["cardinality"] = cardinality
    
    myBn = bayesnet.BayesNet()
    myBn.generateRandomBayesNet(n=nnodes, cardinality=cardinality)

    noiseBn = bayesnet.BayesNet()
    noiseBn.copyGraphFrom(myBn)

    learnedBn = bayesnet.BayesNet()
    learnedBn.copyGraphFrom(myBn)

    for samples in samplerange:
        resultLine["samples"] = samples
        print("Generating %i Samples" % samples)
        learnSamples = myBn.randomSamples(samples)

        learnedBn.learn(learnSamples)
        errorTrue = myBn.diff(learnedBn)
        errorTrueKld = myBn.diffKld(learnedBn)
        errorTrueHellinger = myBn.diffHellinger(learnedBn)
        resultLine["errorTrue"] = errorTrue
        resultLine["errorTrueKld"] = errorTrueKld
        resultLine["errorTrueHellinger"] = errorTrueHellinger

        for eps in epsilons:
            resultLine["epsilon"] = eps

            print("BUILDING BNDPF")
            antinoiseBn = bayesnet.BayesNet()
            antinoiseBn.copyGraphFrom(myBn)

            # build the BNDPF
            for nodeName, node in myBn.nodes.items():
                # each of these out_ vertices we have the parent "vertex" and "lie"
                lie_rv = bayesnet.RandomVariable("lie_" + nodeName, [])
                out_rv = bayesnet.RandomVariable("out_" + nodeName, [nodeName, "lie_" + nodeName])
                lie_rv.learnable = False
                out_rv.learnable = False
                out_rv.states = list(node.states)

                # lie CPD
                alpha = calculate_alpha(cardinality, eps)
                lie_rv.addP({}, {True: alpha, False: 1.0-alpha})
                
                # out CPD
                for parentState in node.states:
                    # case lie="Yes"
                    dep = {nodeName: parentState, "lie_" + nodeName: True}
                    # equal distribution for lie=yes
                    p = { s: 1.0/len(node.states) for s in node.states }
                    out_rv.addP(dep, p)

                    # case lie="No"
                    dep = {nodeName: parentState, "lie_" + nodeName: False}
                    # impossible, but the parent's state
                    p = { s: 0 for s in node.states }
                    p[parentState] = 1
                    out_rv.addP(dep, p)

                    antinoiseBn.addNode(out_rv)
                    antinoiseBn.addNode(lie_rv)
            # "bake" this model
            antinoiseBn.harmonize()
            # print(antinoiseBn)

            print("Anonymizing Eps=%lf" % eps)

            anonResult = []
            for d in learnSamples:
                anonResult.append(antinoiseBn.forwardSample(d))
            # anonResult contains full samples of the BNDPF, i.e., includes the original "nodeName", intermediate state "lie_nodeName", and the output random variable of mechanism M "out_nodeName"
            # in practice, we solely use "out_nodeName" and rename it to "nodeName"
            anonSamples = [{node: d["out_" + node] for node in myBn.nodes} for d in anonResult]

            # use a biased estimator "noise-unaware"
            noiseBn.learn(anonSamples)
            errorAnon = myBn.diff(noiseBn)
            errorAnonKld = myBn.diffKld(noiseBn)
            errorAnonHellinger = myBn.diffHellinger(noiseBn)
            resultLine["errorAnon"] = errorAnon
            resultLine["errorAnonKld"] = errorAnonKld
            resultLine["errorAnonHellinger"] = errorAnonHellinger
            resultLine["errorAnonTrueHellinger"] = learnedBn.diffHellinger(noiseBn)

            # compare to a default model
            randomBn = bayesnet.BayesNet()
            randomBn.copyGraphFrom(myBn)
            errorRandom = myBn.diff(randomBn)
            errorRandomKld = myBn.diffKld(randomBn)
            errorRandomHellinger = myBn.diffHellinger(randomBn)
            resultLine["errorRandom"] = errorRandom
            resultLine["errorRandomKld"] = errorRandomKld
            resultLine["errorRandomHellinger"] = errorRandomHellinger
            resultLine["errorRandomTrueHellinger"] = learnedBn.diffHellinger(randomBn)


            # start with the learned noisy model
            antinoiseBn.learn(anonSamples)
            for i in tqdm.trange(emiterations, desc="EM Iteration", leave=False):
                allsamples = []
                for d in tqdm.tqdm(anonSamples, desc="Samples", leave=False):
                    # we need to manipulate the sample. what we are given is NOT "node0", we are given "out_node0"!
                    nd = {}
                    for k in d:
                        nd["out_" + k] = d[k]
                    imputedSample = antinoiseBn.getfullsample(nd)
                    allsamples += imputedSample
                
                antinoiseBn.learn(allsamples)
                errorEm = myBn.diff(antinoiseBn)
                errorEmKld = myBn.diffKld(antinoiseBn)
                errorEmHellinger = myBn.diffHellinger(antinoiseBn)
                resultLine["errorEm"] = errorEm
                resultLine["errorEmKld"] = errorEmKld
                resultLine["errorEmHellinger"] = errorEmHellinger
                resultLine["errorEmTrueHellinger"] = learnedBn.diffHellinger(antinoiseBn)
                resultLine["emIteration"] = i
                experimentResults.append(dict(resultLine))

    return experimentResults

"""" Experiment Design
Two types of experiments are executable with this evaluation:
1. Analyzing how EM iterations improve the accuracy of learned models through an unbiased estimator
2. Analyzing how different epsilons affect the accuracy of learned models through unbiased and biased estimators
"""


if __name__ == "__main__":
    # INIT
    parser = argparse.ArgumentParser(description='Evaluate DP using EM.')
    parser.add_argument("--name", type=str, help="Name of the experiment.")
    parser.add_argument("-s", "--samples", nargs='+', type=int, help="How many samples to use. Can be list.")
    parser.add_argument("-em", "--emiterations", type=int, help="How many EM iterations")
    parser.add_argument("-eps", "--epsilons", nargs='+', type=float, help="What epsilons to use for noising. Can be list.")
    parser.add_argument("-n", "--nodes", type=int, help="How many nodes to use")
    parser.add_argument("-c", "--cardinality", type=int, help="Cardinality")
    parser.add_argument("-e", "--experiments", type=int, help="How many experiments (different bns) to run")

    args = parser.parse_args()

    for e in range(args.experiments):
        print("RUNNING EXPERIMENT %i" % e)
        experimentResults = runExperiment(args.samples, args.emiterations, args.epsilons, args.nodes, args.cardinality)
        df = pd.DataFrame.from_dict(experimentResults)
        df.to_csv("experiment-" + str(args.name) + "-" + str(e) + "-nodes-" + str(args.nodes) + "-card-" + str(args.cardinality) + "-eps-" + str(args.epsilons) + "-samples-" + str(args.samples) + "-em-" + str(args.emiterations) + ".csv", sep='\t')

