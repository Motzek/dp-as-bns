---
title: |
  | Formalizing Differential Privacy as Bayesian Networks
author: "| Dr. Alexander Motzek \n| Data Insight Lab \n| Lufthansa Industry Solutions\n"
date: "February 2018"
output:
  pdf_document: default
  html_document: default
geometry: margin=1.35in
documentclass: article
classoption: a4paper
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(readr)
require(caret)

require(ggplot2)

library(pastecs)

library(broom)
library(RSEIS) # spectro
library(data.table)  
library(magrittr)
library(dplyr)

```


```{r, include=FALSE}
readWithName <- function(f) {
    df = read_tsv(f, col_names = T)
    df$experiment = f
    return(df)
}

files <- list.files(path="kdd-evals", pattern = glob2rx("experiment-em*.csv"), full.names = T)
tempSingle <- lapply(files, readWithName)
dfemSingle <- rbindlist( tempSingle,  use.names=T )


dupZeros = dfemSingle[emIteration==0] 
dupZeros$emIteration=-1
dupZeros$errorEm = dupZeros$errorAnon
dupZeros$errorEmKld = dupZeros$errorAnonkld
dupZeros$errorEmHellinger = dupZeros$errorAnonHellinger
dfemSingle <- rbind( dfemSingle,  dupZeros, fill=T )

dfemSingle$emIteration = dfemSingle$emIteration+1

dfemSingle$nc = interaction(dfemSingle$nodes, dfemSingle$cardinality)

# errorAnon: Absolute error of naively trained BN from anonymized data using a biased estimator
# errorRandom: Absolute error of a randomly intantiated BN
# errorEm: Absolute error of an unbiased estimator for learning a BN from anonymized data, given for each emIteration
# errorTrue: Absolute error of a BN learned from the underlying unanonymized data

dfemSingle$errorAnonPerc = dfemSingle$errorAnonHellinger #/ dfemSingle$errorTrueHellinger
dfemSingle$errorRandomPerc = dfemSingle$errorRandomHellinger #/ dfemSingle$errorTrueHellinger
dfemSingle$errorEmPerc = dfemSingle$errorEmHellinger #/ dfemSingle$errorTrueHellinger

```

```{r, dpi=600, fig.height=4, fig.width=15, echo=FALSE, message=FALSE}
#ggplot(df, aes(x=samples, y=errorTrue, group=interaction(experiment,epsilon))) + geom_line(alpha=0.3, aes(color = factor(epsilon))) + xlab("# Lerndaten") + ylab("Abweichung des Modells") + scale_x_log10() 

dfsubSingle = dfemSingle[epsilon==1.0]

ggplot(dfsubSingle, aes(x=jitter(emIteration, amount=0.0001), y=errorAnonPerc, group=interaction(nodes, cardinality))) + geom_smooth(linetype="dotted",aes(color = interaction(nodes, cardinality))) + xlab("emIteration") + ylab("Abweichung des Modells")  +geom_smooth(aes(y=errorEmPerc,color = interaction(nodes, cardinality))) + geom_smooth(linetype="dashed", aes(y=errorTrueHellinger,color = interaction(nodes, cardinality))) #+ scale_y_log10()

for(n in unique(dfsubSingle$nodes)){
  df = dfsubSingle[nodes==n]
  dfagg = aggregate(df, by=list(df$emIteration,df$nodes,df$cardinality), FUN=mean, na.rm=TRUE)
  write.table(dfagg, file=paste("single-node-agg-over-emiteration-nodes-",n,".csv",sep=""), quote=F,sep="\t")
}

```


```{r, dpi=600, fig.height=4, fig.width=15, echo=FALSE, message=FALSE}
#ggplot(df, aes(x=samples, y=errorTrue, group=interaction(experiment,epsilon))) + geom_line(alpha=0.3, aes(color = factor(epsilon))) + xlab("# Lerndaten") + ylab("Abweichung des Modells") + scale_x_log10() 

dfsubSingle = dfemSingle[emIteration==25]

ggplot(dfsubSingle, aes(x=jitter(epsilon, amount=0.0001), y=errorAnonPerc, group=interaction(nodes, cardinality))) + geom_smooth(linetype="dotted",aes(color = interaction(nodes, cardinality))) + xlab("Epsilon") + ylab("Abweichung des Modells") +geom_smooth(aes(y=errorEmPerc,color = interaction(nodes, cardinality))) + scale_y_log10()

for(n in unique(dfsubSingle$nodes)){
  df = dfsubSingle[nodes==n]
  dfagg = aggregate(df, by=list(df$epsilon,df$nodes,df$cardinality), FUN=mean, na.rm=TRUE)
  write.table(dfagg, file=paste("single-node-agg-over-epsilon-nodes-",n,".csv",sep=""), quote=F)
}

```
