@echo off
rem Konvergenzverhalten durch EM 
start eval-app-em.py --name em12  --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 2  -e 30
start eval-app-em.py --name em13  --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 3  -e 30
start eval-app-em.py --name em14  --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 4  -e 30
start eval-app-em.py --name em15  --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 5  -e 30
start eval-app-em.py --name em110 --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 10 -e 30
start eval-app-em.py --name em120 --samples 1000 -em 25 -eps 0.01 0.1 0.4 0.5 0.7 0.8 0.9 1 1.5 2 2.5 -n 1 -c 20 -e 30
