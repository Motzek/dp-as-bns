#!python3
# -*- coding: utf-8 -*-
# pylint: disable=W0312, line-too-long, C0103
"""
An implementation of Bayesian networks
Written in Python with a focus on clean code, instead of fast execution

Alexander Motzek, Data Insight Lab, Lufthansa Industry Solutions
"""
import random
import itertools as it
import json
import math
import copy

class ImpossibleSample(Exception):
    pass



def dh(d):
    """generates hash of a dictionary d"""
    return hash(frozenset(d.items()))

def sampleFromDistribution(p):
    """
        p = {"s1": 0.1, "s2": 0.3, "s3": 0.6}
        samples "s1", "s2", or "s3" according to the given distribution
    """
    # print(p.values())
    # simple, but veeeeeeeeeeeeeeeeeeeeeeery slow (about 20x slower!)
    # return np.random.choice(list(p.keys()), size=None, replace=False, p=list(p.values()))

    c = random.random() # candidate
    u = 0               # upper bound
    for key, pr in p.items():
        u += pr
        if c < u:   # candidate c from random.random() is [0,1), i.e., 1.0 is impossible. p may be 1.0. Therefore < is correct
            return key
    
    raise RuntimeError("Sampling from p did not choose an instantiation")

def normalizeDistribution(p):
    """
        p = {"s1": 0.1, "s2": 0.3, "s3": 0.6}
        normalizes p s.t. sum p = 1
    """   

    csum = sum( list(p.values()) )
    # print(csum)
    if csum>0:
        return { key: val/csum for key, val in p.items() }
    else:
        # default to even distribution
        return { key: 1/len(p) for key in p }

class RandomVariable():
    def __init__(self, name, parents=[]):
        """ A new random variable.
            Requires a name and an array of parents (can be added later).
            Assignable to some values defined as .states.
        """
        self.name = name    #str
        self.states = [True, False]  #[str/obj]
        
        if parents is None:
            parents = []

        self.parents = parents #[str]
        self.cpd = {}   # dict of dependencies of dict states
        self.cpd_hash = {}  # dict of hashes to their meaning
        self.learnable = True
        # TODO align name, parents and cpd

    def clear(self, laplace=1):
        """ resets cpds to zeros """
        if not self.learnable:
            return
        for k in self.cpd:
            for j in self.cpd[k]:
                self.cpd[k][j] = laplace

    def learn(self, d):
        """ learns the cpd of this random variable from a dataset d which is an array of dicts as
            d = [ {"X":1, "Y":2, "Z":3}, {"X":0, "Y":7, "Z":1} ]
            respects probabilities of data points by looking into the "__probability" key (useful for EM learning, gibbs sampling, likelihood weighting, etc.)
            only learns this random variable if it is learnable
        """
        # extract the dependencies, i.e., parents
        if not self.learnable:
            return
        if "__probability" in d:
            weight = d["__probability"]
        else:
            weight = 1
        
        relevantInstantiation = self.reduceToRelevantVariablesWithoutSelf(d)
        if relevantInstantiation or not self.parents:   # priors (empty parents) must be learnt as well!
            self.cpd[dh(relevantInstantiation)][d[self.name]] += weight
            self.cpd_hash[dh(relevantInstantiation)] = relevantInstantiation

    def addP(self, parents, p):
        """P(X|Y=2,Z=3)=p,
           where parents={Y":2, "Z":3}
           and p = {state1: val, state2: val}
        """
        if parents is None:
            parents = {}
        # TODO add checks that supplied probability distribution p matches self.states
        # check that supplied parents match this node's parents
        if not set(parents.keys()).issubset(self.parents):
            raise RuntimeError("Dependencies of this CPD do not match node's parents.")
        self.cpd[dh(parents)] = p
        self.cpd_hash[dh(parents)] = parents

    def normalize(self):
        """ normalizes all cpds of this random variable """
        for k in self.cpd:
            self.cpd[k] = normalizeDistribution(self.cpd[k])

    def getP(self, inst, reduce=True, ignore_self=False):
        """returns P(X=1|Y=2,Z=3)=p, 
           where inst={"X":1, "Y":2, "Z":3}
           reduces inst to the required variables if desired (default: reduce=True)
           returns a specific value if inst includes an instantiation of this random variable
           ignores the specified value of this random variable in inst if ingore_self=True
        """
        if reduce:
            ninst = self.reduceToRelevantVariablesWithoutSelf(inst)
        else:
            ninst = inst

        if ignore_self:
            inst = dict(inst)
            inst.pop(self.name, None)


        if self.name in inst:
            # return specific singular value
            return self.cpd[dh(ninst)][inst[self.name]]
        else:
            # return distribution
            return self.cpd[dh(ninst)]

    def reduceToRelevantVariablesWithoutSelf(self, inst):        
        return {key:inst[key] for key in self.parents}

    def __str__(self):
        return str(self.__dict__)

    def drawSample(self, parents):
        """draws a sample of from this random variable, with given parents
        """
        p = self.getP(parents)
        return sampleFromDistribution(p)


    def generateRandomDistribution(self, bn, skew=3):
        #parents' combinations
        combinations = [dict(zip(self.parents, prod)) for prod in it.product(*(bn.nodes[rv].states for rv in self.parents))]

        for comb in combinations:
            p = {s: random.random() for s in self.states}
            # skew the distribution (imply high correlation)
            maxKey = max(p, key=p.get)
            p[maxKey] *= skew

            self.addP(comb, p)
        
        self.normalize() # = normalizeDistribution(self.cpd.p)

class BayesNet():
    """
    """
    def __init__(self):
        self.nodes = {}            # dict of objects
        self.simpleNodeOrdering = []     # [str]      # one specific node ordering
        self.nodeOrdering = []     # [str[str]] # stages for variable elimination
        self.allcpds = dict()      # stores all cpds, directly accessible

    def __str__(self):
        return str(self.__dict__)

    def toJSON(self):
        return json.dumps(self.nodes, default=lambda o: o.__dict__, sort_keys=True, indent=4)

    def randomSamples(self, n):
        """ generates n random samples from this network's distribution"""
        samples = []
        for i in range(0,n):
            samples.append(self.randomSample())

        return samples

    def randomSample(self):
        """ generates one (1) random sample from this network's distribution"""
        
        sample = {}
        for node in self.simpleNodeOrdering:
            sample[node] = self.nodes[node].drawSample(sample)
        return sample

    def forwardSample(self, evidence):
        """ generates n random samples from this network's distribution conditioned on evidence using simple forward sampling based on evidence
            NOTE: this is only works IFF variables in evidence are topological "higher" (come before) all other to-be-sampled random variables, e.g., if they are all priors!
        """
        sample = dict(evidence)
        
        for node in self.simpleNodeOrdering:
            if node not in sample:
                sample[node] = self.nodes[node].drawSample(sample)
        return sample        

    def query(self, query, evidence):
        """ query the bayesian network for query given evidence, i.e., calculate P(query|evidence), where
            evidence is a dict {"Z": "s1", "Q": "s3"} and
            query is a dict {"X": "s1", "Y": "s2"} which returns a specific probability
        """
        # TODO IMPLEMENT
        return 0

    def querySingle(self, query, evidence):
        """ queries the bayesian network for a single random variable query given evidence, i.e., calculate p(query|evidence), where
            evidence is a dict {"Z": "s1", "Q": "s3"} and
            query is a single variable name, e.g. "A"
            Uses simple marginalization
            Returns a distribution p with {state: p, state: p, ...}
        """
        # TODO IMPLEMENT


    def getfulljointdistribution(self, evidence):
        '''
        Return the conditional full joint probability distribution conditionted on the provided evidence. Returns a dict with probabilities.
        NOTE: Impossible entries (random variable instantiation that have probability 0 given the evidence) are NOT returned!
        NOTE: Equivalent to getfullsample.
        NOTE: Requires larger amounts of memory, as complete JPD must be stored in memory.
        Useful for inference (marginalization is simple by using a pandas dataframe and simple counting)
        

        evidence is a dict {"Z": "s1", "Q": "s3"} 
        Returns: Every row is a dict of random variables instantiations a key "__probability" representing the probabilitiy of this instantiation given the evidence.
        '''    
        return self.getfullsample(evidence)   

    def getfullsample(self, sample):
        '''
        Generates a full sample from the given sample by calculating the full joint probability given the sample's evidence. Returns a dict with weights, i.e., probabilities.
        NOTE: Impossible entries (random variable instantiation that have probability 0 given the evidence) are NOT returned!
        NOTE: Equivalent to getfullsample.
        NOTE: Requires larger amounts of memory, as complete JPD must be stored in memory.
        Usefull for EM learning.

        sample is a dict {"Z": "s1", "Q": "s3"} 
        Returns: Every row is a dict of random variables instantiations a key "__probability" representing the probabilitiy of this instantiation given the evidence.
        '''
        #get the missing random variables
        missingrvs = set(self.nodes.keys()) - set(sample.keys())

        missingcombinations = [dict(zip(missingrvs, prod)) for prod in it.product(*(self.nodes[rv].states for rv in missingrvs))]
        #we append these missingcombinations to the sample        

        fullsamples = []
        for comb in missingcombinations:
            try:
                fullsample = dict(sample)   #copy
                fullsample.update(comb)     #enrich with missing values
                # fullsample now contains a full instantiation of the complete network, i.e., every cpd is directly given
                p = 0

                for rvName in fullsample:
                    rv = self.nodes[rvName]
                    condP = rv.getP(fullsample)
                    if condP == 0:
                        # this sample constellation is impossible. ignore and continue!
                        raise ImpossibleSample()
                    p += math.log(condP)

                #multiply everything.
                if p:
                    fullsample["__probability"] = math.exp(p)
                else:
                    fullsample["__probability"] = 0.0
                fullsamples.append(fullsample)

            except ImpossibleSample:
                continue

        # normalize this samples' distribution, i.e., all full-samples that are possible under "sample" must sum up to 1!
        csum = sum( [x["__probability"] for x in fullsamples] )
        for s in fullsamples:
            s["__probability"] /= csum

        return fullsamples

    def harmonize(self):
        """ checks that all references to cpds, nodes, etc. are consistent """

        self.allcpds = dict()
        # copy all cpd entries to allcpds dict. this is used for differencing bayesnet

        for nodeName, node in self.nodes.items():
            for instHash, distrib in node.cpd.items():
                thisInst = node.cpd_hash[instHash]
                # print(distrib)
                for nodeState, p in distrib.items():
                    thisInstClone = dict(thisInst)
                    thisInstClone[nodeName] = nodeState
                    self.allcpds[dh(thisInstClone)] = p

        # TODO : Add checks for references of node to its parents
        # TODO : Add checks for references of CPDs to states and their nodes

        self.findNodeOrdering()
        self.findSimpleNodeOrdering()

    def findSimpleNodeOrdering(self):
        """ finds the topological ordering, stored as orderedNodes. required for sampling """

        # make a copy, as we need to modify the structure
        # we store it in a lightweight structure:
        # { node: [parents] }
        # i.e., { node: [incoming edges] }

        g = {}
        for rvName, rv in self.nodes.items():
            g[rvName] = set(rv.parents)    #copies the parents' list! this is very important!

        # this is the ordered graph
        og = []
        while g:    #as long as there are nodes in g
            for rv, parents in g.items():
                parents -= set(og)    #remove the parents that have already been ordered
                if g[rv] != parents:    #TODO REMOVE we only used this for debuggin :)
                    raise RuntimeError("WARN WARN NO COPY PASSED! THIS DOESNT WORK USE DIRECT REFERENCE")
                if not parents:
                    #rv has no parents (anymore)!
                    og.append(rv)  # add to ordered graph!
                    g.pop(rv)      # remove from our graph
                    break          # start iteration again

        self.simpleNodeOrdering = og
        return self.simpleNodeOrdering

    def findNodeOrdering(self):
        """ finds the topological ordering, stored as nodeOrdering. required for sampling 
            useful for variable elimination, as all "stages" of orderings are preserved
        """

        # make a copy, as we need to modify the structure
        # we store it in a lightweight structure:
        # { node: [parents] }
        # i.e., { node: [incoming edges] }

        g = {}
        for rvName, rv in self.nodes.items():
            g[rvName] = set(rv.parents)    #copies the parents' list! this is very important!

        # this is the ordered graph
        og = []
        while g:    #as long as there are nodes in g
            tobeadded = set()
            for rv, parents in g.items():
                if not parents:
                    #rv has no parents (anymore)!
                    tobeadded.add(rv)  # add to ordered graph!
            og.append(list(tobeadded))

            #now remove them from the (unsorted) graph
            #remove the sorted nodes from all parents
            for rv, parents in g.items():
                parents -= tobeadded
            #now actually remove the sorted nodes from the unsorted graph
            for rv in tobeadded:
                g.pop(rv)

        self.nodeOrdering = og
        return self.nodeOrdering

    def learn(self, df, laplace=1):
        """ learn data set d which is an array of dicts """
        
        # clean variables
        for _, node in self.nodes.items():
            node.clear(laplace)

        for d in df:
            #only loop once over data set
            #delegate learning to every random variable
            for _, node in self.nodes.items():
                node.learn(d)

        # normalize
        for _, node in self.nodes.items():
            node.normalize()

    def learnEm(self, df, iterations=50):
        """ learn a dataset df which is an array of dicts with missing instantiations """

        for _ in range(iterations):
            allsamples = []
            for d in df:
                allsamples += self.getfullsample(d)
            self.learn(allsamples)

    def diffHellinger(self, bn):
        """ calculates the hellinger difference between the JPD implied by self against the JPD given by bn.
            only considers random variables of self
        """
        allcombinations = [dict(zip(self.nodes.keys(), prod)) for prod in it.product(*(self.nodes[rv].states for rv in self.nodes))]
        #we append these missingcombinations to the sample        

        d = 0
        for comb in allcombinations:
            p1 = 1.0
            p2 = 1.0
            for rvName in comb:
                rv1 = self.nodes[rvName]
                rv2 = bn.nodes[rvName]
                p1 *= rv1.getP(comb)
                p2 *= rv2.getP(comb)
            f = math.sqrt(p1) - math.sqrt(p2)
            d += f*f

        d = math.sqrt(d) / math.sqrt(2)
        return d



    def diff(self, bn):
        """ calculates the absolute difference of cpds between this bn and a reference bn """
        error = 0
        self.harmonize()
        bn.harmonize()
        # print(self.allcpds)
        for k in self.allcpds:
            error += abs(self.allcpds[k] - bn.allcpds[k])
            # error += self.allcpds[k] * (math.log(self.allcpds[k]) - math.log(bn.allcpds[k]))

        return error

    def diffKld(self, bn):
        """ calculates the absolute difference of cpds between this bn and a reference bn """
        error = 0
        self.harmonize()
        bn.harmonize()
        # print(self.allcpds)
        nearzero = 0.0001
        for k in self.allcpds:
            if self.allcpds[k] == 0:
                t1 = math.log(nearzero)
                # print("Kld results may be unreliable: p(.)==0 --> (math.log(%lf) - math.log(%lf))" % (self.allcpds[k], bn.allcpds[k]))
                # error += self.allcpds[k] * (math.log(nearzero) - math.log(bn.allcpds[k]))
            else:
                t1 = math.log(self.allcpds[k])
            
            if bn.allcpds[k] == 0:
                t2 = math.log(nearzero)
            else:
                t2 = math.log(bn.allcpds[k])
                #error += self.allcpds[k] * (math.log(self.allcpds[k]) - math.log(nearzero))
                
            #error += self.allcpds[k] * (math.log(self.allcpds[k]) - math.log(bn.allcpds[k]))
            error += self.allcpds[k] * (t1 - t2)

        return error


    def addNode(self, rv):
        self.nodes[rv.name] = rv

    # def addCPD(self, cpd):
        # todo add checks
        # self.cpds.add(cpd)

    def copyCpdFrom(self, bn):
        for nodename, node in self.nodes.items():
            node.cpd_hash = dict(bn.nodes[nodename].cpd_hash)
            node.cpd = copy.deepcopy(bn.nodes[nodename].cpd)

    def copy(self):
        """ creates a deep copy of this bayesnet """
        newbn = BayesNet()
        newbn.copyGraphFrom(self, init_random=False)
        newbn.copyCpdFrom(self)
        return newbn

    def copyGraphFrom(self, bn, init_random=True):
        """ instantiates a bayesnet with the same graphical structure as bn, but assigns random probabilities (if init_random=True) """

        # copy the nodes
        self.nodes = {}
        for nodename, node in bn.nodes.items():
            newrv = RandomVariable(str(node.name), list(node.parents))
            newrv.states = list(node.states)
            self.addNode(newrv)

        if init_random:
            for nodename, node in self.nodes.items():
                node.generateRandomDistribution(self)

        self.harmonize()

        return self


    def generateRandomBayesNet(self, n, cardinality=2):
        """ generates a randomly, *fully connected*, Bayesian network with n nodes, each with cardinality (#states) cardinality.
            NOTE: Above n>10 memory requirements fully explode!
        """
        self.__init__()
        for i in range(0,n):
            newNodeName = "Node" + str(i)
            newRandomVariable = RandomVariable(newNodeName, [])
            for existingRandomVariableName in self.nodes:
                # add the new one as children
                newRandomVariable.parents.append(existingRandomVariableName)

                # if i==n-1:
                    # break

            newRandomVariable.states = ["s" + str(c) for c in range(0,cardinality)]

            
            self.addNode(newRandomVariable)
            # self.orderedNodes.append(newRandomVariable.name)
            newRandomVariable.generateRandomDistribution(self)
            
        self.findNodeOrdering()
        self.findSimpleNodeOrdering()

        return self
