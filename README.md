# Formalizing Differential Privacy as Bayesian Networks

Alexander Motzek, Data Insight Lab, alexander.motzek@lhind.dlh.de

Supplementary material for reproducing experiments executed in "Formalizing Differential Privacy as Bayesian Networks" submitted for review to KDD 2018.

# Expectation Maximization Experiments

All experiments where run by using ```eval-app-em.py``` as described in the corresponding paper.

	>eval-app-em.py --help
	usage: eval-app-em.py [-h] [--name NAME] [-s SAMPLES [SAMPLES ...]]
						  [-em EMITERATIONS] [-eps EPSILONS [EPSILONS ...]]
						  [-n NODES] [-c CARDINALITY] [-e EXPERIMENTS]

	Evaluate DP using EM.

	optional arguments:
	  -h, --help            show this help message and exit
	  --name NAME           Name of the experiment.
	  -s SAMPLES [SAMPLES ...], --samples SAMPLES [SAMPLES ...]
							How many samples to use. Can be list.
	  -em EMITERATIONS, --emiterations EMITERATIONS
							How many EM iterations
	  -eps EPSILONS [EPSILONS ...], --epsilons EPSILONS [EPSILONS ...]
							What epsilons to use for noising. Can be list.
	  -n NODES, --nodes NODES
							How many nodes to use
	  -c CARDINALITY, --cardinality CARDINALITY
							Cardinality
	  -e EXPERIMENTS, --experiments EXPERIMENTS
							How many experiments (different bns) to run

For convenvience three executable ```start-eval-kdd-*.bat``` files are given which start all experiments in parallel.

The implementation is based upon a simple implementation of Bayesian networks by ```bayesnet.py```, which is sufficient to execute all experiments presented in the paper. Effectively, also the anonymization is performed by simple forward sampling (e.g., through the implemented ```forwardSample()``` method) based upon the BNDPF "antinoisebn". Note that, all experiments could also have been executed by any Bayesian networ or PGM implementation such as Pomegranate, Murphy's BNT, Hugin or bnlearn. 

# Experiment Result Analysis

```kdd-evals.Rmd``` analyzes all generated experiments, aggregates them and produces averaged results for different number of random variables in the experiments. 

All aggregated outputs are given in folder ```agg-output```. All individual experiments' outputs are given in ```output```. A single experiment is given as an example in ```output/experiment-em15-0-nodes-1-card-5-eps-[0.01, 0.1, 0.4, 0.5, 0.7, 0.8, 0.9, 1.0, 1.5, 2.0, 2.5]-samples-[1000]-em-25.csv```.